from common import Report, Writer, Reader, PandasFormatter

if __name__ == "__main__":
    report_joined = Report(reader=Reader, writer=Writer, formatter=PandasFormatter())
    report_joined.create_report("sources", "report_joined.csv")
    report_splited = Report(reader=Reader, writer=Writer, formatter=PandasFormatter(join_amount=False))
    report_splited.create_report("sources", "report_splited.csv")
