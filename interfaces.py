class BaseWriter:
    def write(self):
        raise NotImplementedError


class BaseReader:
    def get_source_list(self):
        raise NotImplementedError


class BaseFormatter:
    def process(self):
        raise NotImplementedError


class BaseReport:
    def create_report(self):
        raise NotImplementedError
