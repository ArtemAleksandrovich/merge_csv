# template fields names
DATE_FIELD_NAME = "operation_date"
AMOUNT_FIELD_NAME = "amount"
DESTINATION_FIELD = "to"
SOURCE_FIELD = "from"
CURRENCY = "currency"
TRANSACTION_TYPE = "transaction_type"
CHECK_FIELDS = ["euro", "cents", "amount"]
AMOUNT_INT = "amount eur"
AMOUNT_DEC = "amount cents"

# mapped lowercase expected fieldnames
MAPPING = {
    DATE_FIELD_NAME: {"date_readable", "timestamp", "date"},
    AMOUNT_FIELD_NAME: {"amounts", "amount", "euro", "cents"},
    AMOUNT_INT: {"amount euro", "euro"},
    AMOUNT_DEC: {"amount cents", "cents"},
    DESTINATION_FIELD: {"to"},
    SOURCE_FIELD: {"from"},
    CURRENCY: {"currency"},
    TRANSACTION_TYPE: {"type", "transaction"},
}
