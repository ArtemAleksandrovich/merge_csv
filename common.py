import os
from concurrent.futures import ThreadPoolExecutor, as_completed
from math import trunc
from config import *
import pandas as pd
from copy import copy
from interfaces import *


class Reader:
    def __init__(self, folder_name, file_format=".csv"):
        self.path = os.path.join(os.getcwd(), folder_name)
        self.format = file_format

    def get_source_list(self):
        file_list = os.listdir(self.path)
        file_list = [
            os.path.join(self.path, file) for file in file_list if os.path.splitext(file)[-1] == self.format
        ]
        return file_list


class Writer(BaseWriter):
    """Realise interface method write"""
    def __init__(self, dataframe):
        self.dataframe = dataframe

    def write(self, file_name):
        self.dataframe.to_csv(file_name)


class Report:
    def __init__(self, reader, writer, formatter):
        self.reader = reader
        self.formatter = formatter
        self.writer = writer

    def create_report(self, folder_name, file_name):
        reader = self.reader(folder_name)
        filelist = reader.get_source_list()
        dataframes = self._process_files(filelist)
        aggregated_frame = pd.concat(dataframes)
        ordered_by_date_frame = aggregated_frame.sort_values(by=aggregated_frame.columns[0])
        self.writer(ordered_by_date_frame).write(file_name)

    def _process_files(self, filelist):
        results = []
        with ThreadPoolExecutor() as executor:
            futures = [executor.submit(self.formatter.process, filename) for filename in filelist]
            for future in as_completed(futures):
                if exception := future.exception():
                    raise exception
                results.append(future.result())
        return results


class PandasFormatter(BaseFormatter):

    def __init__(self, check_fields=CHECK_FIELDS, fields=MAPPING, join_amount=True):
        self.fields = fields
        self.join_amount = join_amount
        self.check_fields = check_fields

    @staticmethod
    def _format_date(df):
        from pandas.errors import ParserError
        for column in df.columns[df.dtypes == 'object']:
            try:
                df[column] = pd.to_datetime(df[column])
            except (ParserError, ValueError):
                pass
            else:
                break
        return df

    @staticmethod
    def _read_csv(*args, **kwargs):
        return PandasFormatter._format_date(pd.read_csv(*args, **kwargs))

    def _check_splited(self, df):
        fields = set(self.check_fields)
        check = [column in fields for column in df.columns if column in fields]
        return len(check) > 1

    def process(self, filename):
        dataframe = self._read_csv(filename)
        flag = self._check_splited(dataframe)
        dataframe = self._process_amount(dataframe, flag)
        dataframe = self._reformat_headers(dataframe, flag)
        return dataframe

    @staticmethod
    def _to_new_format(amount):
        int_part = trunc(amount)
        cent_part = int(round(((amount % 1) * 100), 3))
        return int_part, cent_part

    def _process_amount(self, df, flag):
        columns_to_process = df.columns[df.columns.str.contains('|'.join(self.check_fields))]
        if not (flag or self.join_amount):
            df = self._split_amount(df, columns_to_process, self._to_new_format)
            df.drop(columns_to_process, axis=1, inplace=True)
        elif flag and self.join_amount:
            df = self._join_amount(df, columns_to_process)
            df.drop(columns_to_process, axis=1, inplace=True)
        return df

    @staticmethod
    def _split_amount(df, columns_to_process, proceed_function):
        assert len(columns_to_process) == 1
        df['amount euro'], df['amount cents'] = zip(*df[columns_to_process[0]].map(proceed_function))
        return df

    @staticmethod
    def _join_amount(df, columns_to_process):
        assert len(columns_to_process) == 2
        df[AMOUNT_FIELD_NAME] = round(df[columns_to_process[0]] + df[columns_to_process[-1]] / 100, 2)
        return df

    def _reformat_headers(self, df, flag):
        fields_dict = copy(self.fields)
        if flag and not self.join_amount:
            fields_dict.pop(AMOUNT_FIELD_NAME)
        new_headers = []
        for header in df.columns:
            for key, values in fields_dict.items():
                if header in values:
                    new_headers.append(key)
        df.columns = new_headers
        # reorder_columns
        df = (df.reindex(MAPPING.keys(), axis=1).dropna(how='all', axis=1))
        return df








